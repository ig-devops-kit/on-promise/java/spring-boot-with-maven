# Spring Boot with Maven
Spring Boot - Hello World 애플리케이션 Maven CI/CD 파이프라인 예제
#### 환경
- java 17
- Maven 
- Spring Boot 3.0.2
- Dependency
  - Spring Boot web 
  - devtools starters
  - Project Lombok
  - Spring Boot test starter


#### 파이프라인 정보
- **Maven Basic** 
  [maven:build, maven:test]
- **Spring Boot Maven with SSH JAR** 
  [maven:build, maven:test, security:semgrep-sast, security:code-quality, security:secret_detection, deploy:jar]
- **Spring Boot Maven with Docker Compose**
  [maven:build, maven:test, security:semgrep-sast, security:code-quality, security:secret_detection, security:container_scanning, docker:kaniko, deploy:docker-compose]
