## Build
FROM maven:3.8.3-openjdk-17 as builder
WORKDIR /app
COPY pom.xml ./
COPY src src
RUN mvn clean package -DskipTests

## Deploy
FROM eclipse-temurin:17-jre-alpine
VOLUME /tmp
COPY --from=builder /app/target/*.jar app.jar
ENTRYPOINT ["sh", "-c", "java -jar /app.jar"]
